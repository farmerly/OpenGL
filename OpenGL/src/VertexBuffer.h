#pragma once

class VertexBuffer
{
public:
    VertexBuffer(const void *data, unsigned int size);
    ~VertexBuffer();

public:
    void bind() const;
    void unbind() const;

private:
    unsigned int m_rendererId;
};
