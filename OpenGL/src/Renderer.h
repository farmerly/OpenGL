#pragma once

#define GLUT_3_3_CORE_PROFILE
#include <GL/glew.h>

#include "IndexBuffer.h"
#include "Shader.h"
#include "VertexArray.h"

#define ASSERT(x) \
    if (!(x))     \
        __debugbreak();

#define GLCall(x)   \
    GLClearError(); \
    x;              \
    ASSERT(GLLogCall(#x, __FILE__, __LINE__))

void GLClearError();
bool GLLogCall(const char *function, const char *file, int line);

class Renderer
{
public:
    Renderer();
    ~Renderer();

public:
    void clear() const;
    void setClearColor(float red, float green, float blue, float alpha) const;
    void draw(const VertexArray &va, const IndexBuffer &ib, const Shader &shader) const;
};