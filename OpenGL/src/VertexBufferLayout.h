#pragma once

#include <GL/glew.h>
#include <vector>

#include "Renderer.h"

struct VertexBufferElement {
    unsigned int  type;
    unsigned int  count;
    unsigned char normalized;

    static unsigned int getSizeOfType(unsigned int type)
    {
        switch (type) {
            case GL_FLOAT:
                return sizeof(GLfloat);
            case GL_UNSIGNED_INT:
                return sizeof(GLuint);
            case GL_UNSIGNED_BYTE:
                return sizeof(GLbyte);
        }
        ASSERT(false);
        return 0;
    }
};

/**
 * 在 Visual Studio 2022 版本 17.1 及更高版本中，如果与 static_assert 关联的表达式不是依赖性表达式，则编译器会在分析表达式时对其进行计算。
 * 如果表达式的计算结果为 false，编译器将发出错误。以前，如果 static_assert
 * 在一个函数模板的主体内（或在一个类模板的成员函数的主体内），编译器就不会执行这种分析。
 *
 * 这是一项源中断性变更。 它适用于任何表示 /permissive- 或 /Zc:static_assert 的模式。 可以使用 /Zc:static_assert-
 * 编译器选项来禁用此行为更改。要解决此问题，需要使表达式具有依赖性。
 */
template <typename>
constexpr bool dependent_false = false;

class VertexBufferLayout
{
public:
    VertexBufferLayout() : m_stride(0){};
    ~VertexBufferLayout(){};

public:
    template <typename T>
    void push(unsigned int count)
    {
        static_assert(dependent_false<T>, "always false");
    }

    template <>
    void push<float>(unsigned int count)
    {
        m_elements.push_back({GL_FLOAT, count, GL_FALSE});
        m_stride += VertexBufferElement::getSizeOfType(GL_FLOAT) * count;
    }

    template <>
    void push<unsigned int>(unsigned int count)
    {
        m_elements.push_back({GL_UNSIGNED_INT, count, GL_FALSE});
        m_stride += VertexBufferElement::getSizeOfType(GL_UNSIGNED_INT) * count;
    }

    template <>
    void push<unsigned char>(unsigned int count)
    {
        m_elements.push_back({GL_UNSIGNED_BYTE, count, GL_TRUE});
        m_stride += VertexBufferElement::getSizeOfType(GL_UNSIGNED_BYTE) * count;
    }

    inline const std::vector<VertexBufferElement> getElements() const
    {
        return m_elements;
    }

    inline unsigned int getStride() const
    {
        return m_stride;
    }

private:
    std::vector<VertexBufferElement> m_elements;
    unsigned int                     m_stride;
};