#pragma once

#include <functional>
#include <string>
#include <vector>

namespace test {
class Test
{
public:
    Test() = default;
    virtual ~Test() = default;

public:
    virtual void onUpdate(float deltaTime){};
    virtual void onRender(){};
    virtual void onImGuiRender(){};
};

class TestMenu : public Test
{
public:
    TestMenu(Test *&currentTestPointer);

public:
    void onImGuiRender() override;

    template <typename T>
    void registerTest(const std::string &name)
    {
        m_vecTests.push_back(std::make_pair(name, []() {
            return new T();
        }));
    }

private:
    Test *&m_currentTest;

    std::vector<std::pair<std::string, std::function<Test *()>>> m_vecTests;
};
} // namespace test