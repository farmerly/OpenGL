#include "TestTexture2D.h"

#include "Renderer.h"
#include "glm/gtc/matrix_transform.hpp"
#include "imgui/imgui.h"
#include <GL/glew.h>

namespace test {

TestTexture2D::TestTexture2D() :
    m_proj(glm::ortho(0.0f, 960.0f, 0.0f, 540.0f, -1.0f, 1.0f)), m_view(glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0))),
    m_translationA(200, 200, 0), m_translationB(400, 200, 0)
{
    float positions[] = {
        -50.0f,
        -50.0f,
        0.0f,
        0.0f,
        50.0f,
        -50.0f,
        1.0f,
        0.0f,
        50.0f,
        50.0f,
        1.0f,
        1.0f,
        -50.0f,
        50.0f,
        0.0f,
        1.0f,
    };

    unsigned int indices[] = {0, 1, 2, 2, 3, 0};

    GLCall(glEnable(GL_BLEND));
    GLCall(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));

    m_vao = std::make_unique<VertexArray>();
    m_vbo = std::make_unique<VertexBuffer>(positions, 4 * 4 * sizeof(float));
    VertexBufferLayout layout;
    layout.push<float>(2);
    layout.push<float>(2);
    m_vao->addBuffer(*m_vbo, layout);
    m_ibo = std::make_unique<IndexBuffer>(indices, 6);

    m_shader = std::make_unique<Shader>("res/shaders/Basic.shader");
    m_shader->bind();
    m_shader->setUniform4f("u_color", 0.2f, 0.3f, 0.8f, 1.0f);
    m_texture = std::make_unique<Texture>("res/textures/ChernoLogo.png");
    m_shader->setUniform1i("u_texture", 0);

    m_vao->unbind();
    m_vbo->unbind();
    m_ibo->unbind();
    m_shader->unbind();
}

TestTexture2D::~TestTexture2D()
{
}

void TestTexture2D::onUpdate(float deltaTime)
{
}

void TestTexture2D::onRender()
{
    GLCall(glClearColor(0.0f, 0.0f, 0.0f, 1.0f));
    GLCall(glClear(GL_COLOR_BUFFER_BIT));

    Renderer renderer;
    m_texture->bind();

    {
        glm::mat4 model = glm::translate(glm::mat4(1.0f), m_translationA);
        glm::mat4 mvp = m_proj * m_view * model;
        m_shader->bind();
        m_shader->setUniformMat4("u_mvp", mvp);
        renderer.draw(*m_vao, *m_ibo, *m_shader);
    }

    {
        glm::mat4 model = glm::translate(glm::mat4(1.0f), m_translationB);
        glm::mat4 mvp = m_proj * m_view * model;
        m_shader->bind();
        m_shader->setUniformMat4("u_mvp", mvp);
        renderer.draw(*m_vao, *m_ibo, *m_shader);
    }
}

void TestTexture2D::onImGuiRender()
{
    ImGui::SliderFloat3("TranslationA", &m_translationA.x, 0.0f, 960.0f);
    ImGui::SliderFloat3("TranslationB", &m_translationB.x, 0.0f, 960.0f);
    ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
}

} // namespace test