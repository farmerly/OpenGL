#pragma once

#include <memory>

#include "Test.h"
#include "Texture.h"
#include "VertexBuffer.h"
#include "VertexBufferLayout.h"
#include "glm/glm.hpp"

namespace test {
class TestTexture2D : public Test
{
public:
    TestTexture2D();
    ~TestTexture2D();

public:
    void onUpdate(float deltaTime) override;
    void onRender() override;
    void onImGuiRender() override;

private:
    glm::vec3 m_translationA;
    glm::vec3 m_translationB;

    std::unique_ptr<VertexArray>  m_vao;
    std::unique_ptr<VertexBuffer> m_vbo;
    std::unique_ptr<IndexBuffer>  m_ibo;
    std::unique_ptr<Shader>       m_shader;
    std::unique_ptr<Texture>      m_texture;

    glm::mat4 m_proj, m_view;
};
} // namespace test