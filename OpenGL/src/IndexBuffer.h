#pragma once

class IndexBuffer
{
public:
    IndexBuffer(const unsigned int *data, unsigned int count);
    ~IndexBuffer();

public:
    void         bind() const;
    void         unbind() const;
    unsigned int getCount() const
    {
        return m_indexCount;
    }

private:
    unsigned int m_rendererId;
    unsigned int m_indexCount;
};
