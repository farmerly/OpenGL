#pragma once

#include <string>
#include <unordered_map>

#include "glm/glm.hpp"

struct ShaderProgramSource {
    std::string vertexSource;
    std::string fragmentSource;
};

class Shader
{
public:
    Shader(const std::string &filepath);
    ~Shader();

public:
    void bind() const;
    void unbind() const;

    // set uniform
    void setUniform1i(const std::string &name, int value);
    void setUniform1f(const std::string &name, float value);
    void setUniform2f(const std::string &name, float v0, float v1);
    void setUniform3f(const std::string &name, float v0, float v1, float v2);
    void setUniform4f(const std::string &name, float v0, float v1, float v2, float v3);
    void setUniformMat3(const std::string &name, const glm::mat3 &matrix);
    void setUniformMat4(const std::string &name, const glm::mat4 &matrix);

private:
    int                 getUniformLocation(const std::string &name);
    ShaderProgramSource parseShader(const std::string &filepath);
    unsigned int        compileShader(unsigned int type, const std::string &source);
    unsigned int        createShader(const std::string &vertexShader, const std::string &fragmentShader);

private:
    unsigned int                         m_rendererId;
    std::string                          m_filepath;
    std::unordered_map<std::string, int> m_uniformLocationCache;
};
