#include "Shader.h"

#include <fstream>
#include <iostream>
#include <sstream>

#include "Renderer.h"

Shader::Shader(const std::string &filepath) : m_filepath(filepath), m_rendererId(0)
{
    ShaderProgramSource source = parseShader(filepath);
    m_rendererId = createShader(source.vertexSource, source.fragmentSource);
}

Shader::~Shader()
{
    GLCall(glDeleteProgram(m_rendererId));
}

void Shader::bind() const
{
    GLCall(glUseProgram(m_rendererId));
}

void Shader::unbind() const
{
    GLCall(glUseProgram(0));
}

void Shader::setUniform1i(const std::string &name, int value)
{
    int location = getUniformLocation(name);
    GLCall(glUniform1i(location, value));
}

void Shader::setUniform1f(const std::string &name, float value)
{
    int location = getUniformLocation(name);
    GLCall(glUniform1f(location, value));
}

void Shader::setUniform2f(const std::string &name, float v0, float v1)
{
    int location = getUniformLocation(name);
    GLCall(glUniform2f(location, v0, v1));
}

void Shader::setUniform3f(const std::string &name, float v0, float v1, float v2)
{
    int location = getUniformLocation(name);
    GLCall(glUniform3f(location, v0, v1, v2));
}

void Shader::setUniform4f(const std::string &name, float v0, float v1, float v2, float v3)
{
    int location = getUniformLocation(name);
    GLCall(glUniform4f(location, v0, v1, v2, v3));
}

void Shader::setUniformMat3(const std::string &name, const glm::mat3 &matrix)
{
    int location = getUniformLocation(name);
    GLCall(glUniformMatrix3fv(location, 1, GL_FALSE, &matrix[0][0]));
}

void Shader::setUniformMat4(const std::string &name, const glm::mat4 &matrix)
{
    int location = getUniformLocation(name);
    GLCall(glUniformMatrix4fv(location, 1, GL_FALSE, &matrix[0][0]));
}

int Shader::getUniformLocation(const std::string &name)
{
    if (m_uniformLocationCache.find(name) != m_uniformLocationCache.end())
        return m_uniformLocationCache[name];

    GLCall(int location = glGetUniformLocation(m_rendererId, name.c_str()));
    if (location == -1)
        std::cout << "warning: uniform '" << name << "'doesn't exist!" << std::endl;

    m_uniformLocationCache[name] = location;
    return location;
}

ShaderProgramSource Shader::parseShader(const std::string &filepath)
{
    std::ifstream stream(filepath);

    enum class ShaderType {
        NONE = -1,
        VERTEX,
        FRAGMENT
    };

    std::string       line;
    std::stringstream ss[2];
    ShaderType        type = ShaderType::NONE;
    while (getline(stream, line)) {
        if (line.find("#shader") != std::string::npos) {
            if (line.find("vertex") != std::string::npos) {
                // set mode to vertex
                type = ShaderType::VERTEX;
            } else if (line.find("fragment") != std::string::npos) {
                // set mode to fragment
                type = ShaderType::FRAGMENT;
            }
        } else {
            ss[(int)type] << line << '\n';
        }
    }
    return {ss[0].str(), ss[1].str()};
}

unsigned int Shader::compileShader(unsigned int type, const std::string &source)
{
    unsigned int id = glCreateShader(type);
    const char  *src = source.c_str();
    GLCall(glShaderSource(id, 1, &src, nullptr));
    GLCall(glCompileShader(id));

    int result;
    glGetShaderiv(id, GL_COMPILE_STATUS, &result);
    if (result == GL_FALSE) {
        int length;
        GLCall(glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length));
        char *message = (char *)_malloca(length * sizeof(char));
        std::cout << "Failed to compile " << (type == GL_VERTEX_SHADER ? "vertex" : "fragment") << " shader: " << message << std::endl;
        glDeleteShader(id);
        return 0;
    }
    return id;
}

unsigned int Shader::createShader(const std::string &vertexShader, const std::string &fragmentShader)
{
    unsigned int program = glCreateProgram();
    unsigned int vs = compileShader(GL_VERTEX_SHADER, vertexShader);
    unsigned int fs = compileShader(GL_FRAGMENT_SHADER, fragmentShader);

    GLCall(glAttachShader(program, vs));
    GLCall(glAttachShader(program, fs));
    GLCall(glLinkProgram(program));
    GLCall(glValidateProgram(program));

    glDeleteShader(vs);
    glDeleteShader(fs);

    return program;
}